FROM java:8-jre
MAINTAINER Marcus Lacerda <marcus.lacerda@gmail.com>

ADD ./target/technology.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/technology.jar"]

EXPOSE 8082
