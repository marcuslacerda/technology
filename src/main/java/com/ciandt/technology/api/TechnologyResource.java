package com.ciandt.technology.api;

import com.ciandt.technology.domain.Technology;
import com.ciandt.technology.service.TechnologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="technology")
public class TechnologyResource {

    @Autowired
    private TechnologyService service;

    @GetMapping
    public List<Technology> list() {
        return service.findAll();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Technology> get(@PathVariable String id) {
        Optional<Technology> technology = service.findById(id);

        if (technology.isPresent()) {
            return new ResponseEntity(technology.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity((HttpStatus.NOT_FOUND));
        }
    }

    @PostMapping(value = "/{id}")
    public Technology add(@PathVariable String id, @RequestBody Technology tech) {
        return service.addOrUpdate(tech);
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity remove(@PathVariable String id) {
        Optional<Technology> technology = service.findById(id);

        return technology.map(tech -> {
            service.remove(id);
            return new ResponseEntity(HttpStatus.OK);
        }).orElse(new ResponseEntity((HttpStatus.NOT_FOUND)));
    }

}
