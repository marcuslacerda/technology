package com.ciandt.technology.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "people-api")
public interface PeopleApiClient {

    @RequestMapping(method = RequestMethod.GET, value = "/people/{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Person get(@PathVariable("id")  String id);
}
