package com.ciandt.technology.domain;

import java.time.Instant;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@EqualsAndHashCode(of={"id"})
@Getter
@Setter
@org.springframework.data.mongodb.core.mapping.Document(collection = "technology")
public class Technology {

    @Id
    private String id;

    @NotNull
    @Size(min = 1, max = 50)
    @Indexed
    private String name;

    @Size(max = 255)
    private String shortDescription;

    @Size(max = 255)
    private String description;

    @NotNull
    @Field("image_url")
    private String imageUrl;

    @NotNull
    private Instant creationDate;

    @NotNull
    private Instant lastActivity;

    @NotNull
    private Author author;

    public Technology() {}

    public Technology(String id) {
        this.id = id;
    }

    public Technology(String id, String name, String login) {
        this.id = id;
        this.name = name;
        this.author = new Author(login);
    }
}
