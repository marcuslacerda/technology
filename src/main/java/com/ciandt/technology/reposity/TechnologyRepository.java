package com.ciandt.technology.reposity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ciandt.technology.domain.Technology;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TechnologyRepository extends MongoRepository<Technology, String>{

    Optional<Technology> findOneByName(String name);

}
