package com.ciandt.technology.api;

import com.ciandt.technology.TechnologyApp;
import com.ciandt.technology.domain.Technology;
import com.ciandt.technology.service.TechnologyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {TechnologyApp.class})
public class TechnologyResourceIntTest {

    private static final ObjectMapper mapper = new ObjectMapper();


    private MockMvc techologyApi;

    @InjectMocks
    private TechnologyResource techResource;

    @Mock
    private TechnologyService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.techologyApi = MockMvcBuilders.standaloneSetup(techResource).build();
    }

    @Test
    public void shouldGetTechnologies() throws Exception {

        when(service.findAll()).thenReturn(Arrays.asList(
                new Technology("angular","Angular", "mlacerda"),
                new Technology("node","Node JS", "mlacerda")));

        String path = "/technology";

        techologyApi.perform(get(path))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void shouldReturnOKWhenAddNewTechnology() throws Exception {
        String techId = "angularjs";
        Technology tech = new Technology();
        tech.setId(techId);
        tech.setName("Angular JS");
        tech.setShortDescription("Modern web framework");
        String json = mapper.writeValueAsString(tech);

        when(service.addOrUpdate(tech)).thenReturn(tech);

        techologyApi.perform(post("/technology/" + techId).contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(techId))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void shouldReturnStatusNotFoundWhenIdNotExits() throws Exception {
        String techId = "duplitedId";

        when(service.findById(techId)).thenReturn(Optional.empty());

        techologyApi.perform(get("/technology/" + techId))
                .andExpect(status().isNotFound());

    }
}
