package com.ciandt.technology.domain;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TechnologyTest {

    @Test
    public void createNewTechnology() {
        String author = "mlacerda";
        Technology tech = new Technology("angular", "Angular JS", author);

        assertEquals(author, tech.getAuthor().getId());
    }
}
